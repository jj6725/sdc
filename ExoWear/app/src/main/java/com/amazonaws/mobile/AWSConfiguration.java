//
// Copyright 2016 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
//
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to 
// copy, distribute and modify it.
//
// Source code generated from template: aws-my-sample-app-android v0.7
//
package com.amazonaws.mobile;

import com.amazonaws.regions.Regions;

/**
 * This class defines constants for the developer's resource
 * identifiers and API keys. This configuration should not
 * be shared or posted to any public source code repository.
 */
public class AWSConfiguration {

    // AWS MobileHub user agent string
    public static final String AWS_MOBILEHUB_USER_AGENT =
        "MobileHub fa84408c-b5c3-460c-aca5-14c18ec809e8 aws-my-sample-app-android-v0.7";
    // AMAZON COGNITO
    public static final Regions AMAZON_COGNITO_REGION =
        Regions.US_EAST_1;
    public static final String  AMAZON_COGNITO_IDENTITY_POOL_ID =
        "us-east-1:239e9467-3105-4f84-bcaf-96185bc559c5";
    // Custom Developer Provided Authentication ID
    public static final String DEVELOPER_AUTHENTICATION_PROVIDER_ID =
        "exowear.308516786437.com";
    // Developer Authentication - URL for Create New Account
    public static final String DEVELOPER_AUTHENTICATION_CREATE_ACCOUNT_URL =
        "aws.amazon.com";
    // Developer Authentication - URL for Forgot Password
    public static final String DEVELOPER_AUTHENTICATION_FORGOT_PASSWORD_URL =
        "aws.amazon.com";
    // Account ID
    public static final String DEVELOPER_AUTHENTICATION_ACCOUNT_ID =
        "308516786437";
    public static String DEVELOPER_AUTHENTICATION_DISPLAY_NAME = "Custom";
}
