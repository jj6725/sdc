package com.exowear.exowear;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import com.unity3d.player.UnityPlayer;

public class UnityPlayerActivity extends Activity
{
	protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code

	//Declare a FrameLayout object
	FrameLayout fl_forUnity;

	//Declare the buttons
	Button bt_rotLeft;
	Button bt_rotRight;
	Button bt_stop;

	// Setup activity layout
	@Override protected void onCreate (Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy

		mUnityPlayer = new UnityPlayer(this);
		if (mUnityPlayer.getSettings ().getBoolean ("hide_status_bar", true))
		{
			setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
			getWindow ().setFlags (WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

		//setContentView(mUnityPlayer);
		//Set the content to main
		setContentView(R.layout.main);

		//Inflate the frame layout from XML
		this.fl_forUnity = (FrameLayout)findViewById(R.id.fl_forUnity);

		//Add the mUnityPlayer view to the FrameLayout, and set it to fill all the area available
		this.fl_forUnity.addView(mUnityPlayer.getView(),
				FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

		//Initialize the buttons from the XML file
		this.bt_rotLeft = (Button) findViewById(R.id.bt_rotLeft);
		this.bt_rotRight = (Button) findViewById(R.id.bt_rotRight);
		this.bt_stop = (Button) findViewById(R.id.bt_stop);

        /*Send a brodcast a message to the 'Main Camera' game object, passing L, R according to the
        pressed buttons.*/
		this.bt_rotLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UnityPlayer.UnitySendMessage("Main Camera", "ReceiveRotDir", "L");
			}
		});

		this.bt_rotRight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UnityPlayer.UnitySendMessage("Main Camera", "ReceiveRotDir", "R");
			}
		});

		this.bt_stop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UnityPlayer.UnitySendMessage("Main Camera", "ReceiveRotDir", " ");
			}
		});

		mUnityPlayer.requestFocus();
	}

	// Quit Unity
	@Override protected void onDestroy ()
	{
		mUnityPlayer.quit();
		super.onDestroy();
	}

	// Pause Unity
	@Override protected void onPause()
	{
		super.onPause();
		mUnityPlayer.pause();
	}

	// Resume Unity
	@Override protected void onResume()
	{
		super.onResume();
		mUnityPlayer.resume();
	}

	// This ensures the layout will be correct.
	@Override public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		mUnityPlayer.configurationChanged(newConfig);
	}

	// Notify Unity of the focus change.
	@Override public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		mUnityPlayer.windowFocusChanged(hasFocus);
	}

	// For some reason the multiple keyevent type is not supported by the ndk.
	// Force event injection by overriding dispatchKeyEvent().
	@Override public boolean dispatchKeyEvent(KeyEvent event)
	{
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return mUnityPlayer.injectEvent(event);
		return super.dispatchKeyEvent(event);
	}

	// Pass any events not handled by (unfocused) views straight to UnityPlayer
	@Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
	@Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
	/*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }


}
