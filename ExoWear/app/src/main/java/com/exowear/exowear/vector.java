package com.exowear.exowear;

import java.util.ArrayList;

public class vector {
    private int size;
    private ArrayList<Double> list;

    public vector (ArrayList<Double> dims) {
        this.list = dims;
        this.size = dims.size();
    }

    public vector(Quant4d q) {
        this.list = new ArrayList<Double>();
        double angle = Math.acos(q.getW());
        this.list.add(q.getX()/Math.sin(angle));
        this.list.add(q.getY()/Math.sin(angle));
        this.list.add(q.getZ()/Math.sin(angle));
        this.size = 3;
    }

    public vector(double... dims) {
        this.list = new ArrayList<Double>();
        this.size = 0;
        for (double dim : dims) {
            System.out.println(dim);
            this.list.add(dim);
            this.size = size + 1;
        }
    }

    public int size() {
        return this.size;
    }

    public void pushRight(double e) {
        this.list.add(e);
        this.size = this.size + 1;
    }

    public void add (double... elems) {
        for (double e : elems) {
            this.list.add(e);
            this.size = this.size + 1;
        }
    }

    public static Quant4d vtoq(vector v) {
        return new Quant4d(0.0, v.getX(), v.getY(), v.getZ());
    }

    public double get(int idx) {
        return this.list.get(idx);
    }

    public double dot(vector v) {
        if (v.size != this.size) {
            System.out.println("Vectors must be the same size!");
            return -1.0;
        }

        double ret = 0.0;

        for (int i = 0; i < this.size; i++) {
            ret = ret + (this.list.get(i) * v.get(i));
        }

        return ret;
    }

    public double mag() {
        double ret = 0;
        for (double e : this.list) {
            ret = ret + (Math.pow(e,2.0));
        }
        return Math.sqrt(ret);
    }

    public double angleWith(vector v) {
        if (this.size != v.size) {
            System.out.println("Vectors must be the same size!");
            return -1.0;
        }

        double d = this.dot(v)/(this.mag()*v.mag());
        return Math.acos(d);
    }

    public double getX() {
        return this.list.get(0);
    }

    public double getY() {
        return this.list.get(1);
    }

    public double getZ() {
        return this.list.get(2);
    }
}