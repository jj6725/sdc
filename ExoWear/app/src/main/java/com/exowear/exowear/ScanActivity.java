package com.exowear.exowear;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.Manifest;

import com.amazonaws.AmazonClientException;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.user.IdentityManager;
import com.exowear.exowear.demo.nosql.DemoNoSQLTableBase;
import com.exowear.exowear.demo.nosql.DemoNoSQLTableFactory;
import com.unity3d.player.UnityPlayer;

import org.apache.commons.math3.util.FastMath;
import org.w3c.dom.Text;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class ScanActivity extends ListActivity implements IdentityManager.SignInStateChangeListener {

    /** Bundle key for retrieving the table name from the fragment's arguments. */
    public static final String BUNDLE_ARGS_TABLE_TITLE = "ExoWearDB";

    /** The NoSQL Table demo operations will be run against. */
    private DemoNoSQLTableBase demoTable;

    /** The identity manager used to keep track of the current user account. */
    private IdentityManager identityManager;

    /** Variables used for AWS DynamoDB attributes. */
    protected String primaryKey;
    private String userid;
    private String username;
    private double angleMax;
    private double ext_min;
    private double ext_max;
    private double ext_mean;
    private double ext_var;
    private double ext_std;
    private double flx_min;
    private double flx_max;
    private double flx_mean;
    private double flx_var;
    private double flx_std;

    /** Variables used for keeping time (duration of exercise, +primary key). */
    //public int counter = 0;
    private int currentYear;
    private String currentMonth;
    private int currentDay;
    private int currentHr;
    private int currentMin;
    private int startMin;
    private int startSec;
    private int endMin;
    private int endSec;
    public boolean startCount = true;

    /** Log variable for debugging purposes. Can be viewed on Android Monitor by calling Log.w(TAG, "");  */
    private final static String TAG = ScanActivity.class.getSimpleName();

    /** Variables for Bluetooth Low Energy connection. */
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private BleService mBleService;
    private int deviceCount = 0;
    public boolean mConnected = false;
    public boolean mStreaming = false;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 300000;
    private static final int MY_PERMISSION_RESPONSE = 42;

    /** General function variables for the activity. */
    private ArrayList<String> quatDataSet = new ArrayList<String>();
    private ArrayList<Double> angleDataSet = new ArrayList<Double>();
    private ArrayList<Double> extDataSet = new ArrayList<Double>();
    private ArrayList<Double> flexDataSet = new ArrayList<Double>();
    private ArrayList<Double> varDataSet = new ArrayList<Double>();
    private ArrayList<Double> tempDataSet = new ArrayList<Double>();
    private ArrayList<Double> tDataSet1 = new ArrayList<Double>();
    private ArrayList<Double> tDataSet2 = new ArrayList<Double>();

    private AlphaAnimation buttonClick = new AlphaAnimation(1.0F, 0.7F);
    private ProgressBar spinner;
    private ProgressBar statSpinner;
    private LineGraph linegraph;
    private int index;
    private double max;
    private double mean = 0;
    private double var = 0;
    private int timecounter = 0;
    public Button connectBle;
    public Button disconnectBle;
    public Button reconnectBle;
    public Button exerciseView;
    public Button visualView;
    public Button summaryView;
    public Button upperLeg;
    public Button lowerLeg;
    public boolean calibrate = true;
    public boolean executeQuit = false;
    private RelativeLayout state1;
    private RelativeLayout state2;
    public RelativeLayout exerciseLayout;
    public RelativeLayout visualLayout;
    public RelativeLayout summaryLayout;
    private TextView statusText;
    private TextView resultText;
    private TextView resultVal;
    //private TextView dataAngle1Text;
    //private TextView dataAngle2Text;
    //private TextView dataAngleText;
    private TextView scanStatus;

    /** Variables for calibration purposes. Do not remove/modify without consideration. */
    private String quaternion;
    String[] li = new String[4];
    public int val_counter = 0;
    public int device_counter = 0;
    public int scan_counter = 0;

    private int calib_msg_count2 = 0;
    private ArrayList<Quant4d> calib_points2 = new ArrayList<Quant4d>();
    //    private vector start2;
    private Quant4d start2;
    private boolean start_record2 = true;
    private long start_time2;
    private char spinAxis = 'x';
    private int printcount = 0;
    private int printrate = 10;
    private ArrayList<Double> tempDataSet2 = new ArrayList<Double>();
    private ArrayList<Double> angleDataSet2 = new ArrayList<Double>();
    private ArrayList<Quant4d> quatDataSet2 = new ArrayList<Quant4d>();
    private int valCounter2 = 0;

    private String duration;
    private String device1;
    private String device2;
    private Quant4d start;
    private Quant4d start1;
    private boolean start_record1 = true;
    private long start_time1;
    private ArrayList<Double> tempDataSet1 = new ArrayList<Double>();
    private ArrayList<Quant4d> quatDataSet1 = new ArrayList<Quant4d>();
    private int valCounter1 = 0;
    private int calib_msg_count1 = 0;
    private ArrayList<Quant4d> calib_points1 = new ArrayList<Quant4d>();

    private int AVERAGE_RATE = 5;

    private double angle1 = 0;
    private double angle2 = 0;

    /** Variables for Unity Player. */
    protected UnityPlayer mUnityPlayer;
    public FrameLayout fl_forUnity;
    public Button bt_rotLeft;
    public Button bt_rotRight;
    public Button bt_stop;

    /** Variables for SQLite database. */
    private DBHelper exoweardb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFormat(PixelFormat.RGBX_8888);
        setContentView(R.layout.activity_scan);
        mHandler = new Handler();

        spinner = (ProgressBar) findViewById(R.id.progress_bar);
        spinner.setVisibility(View.VISIBLE);
        spinner.getIndeterminateDrawable().setColorFilter(0xFFFFA500,
                android.graphics.PorterDuff.Mode.MULTIPLY);

        statSpinner = (ProgressBar) findViewById(R.id.progress_bar2);
        statSpinner.setVisibility(View.INVISIBLE);
        statSpinner.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF,
                android.graphics.PorterDuff.Mode.MULTIPLY);

        /**
         * Code for obtaining email account information from Signin Activity.
         */
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String email_userid = extras.getString("USER_EMAIL_ACCOUNT");
                email_userid = email_userid.replace("@exowear.co", "");
                username = email_userid;
            }
        } catch (Exception e) {}

        /**
         * Code for Unity Player which is used for animation rendering and visualization.
         */
        mUnityPlayer = new UnityPlayer(this);
        if (mUnityPlayer.getSettings().getBoolean ("hide_status_bar", true))
        {
            setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
            getWindow ().setFlags (WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        this.fl_forUnity = (FrameLayout)findViewById(R.id.unity);

        //Add the mUnityPlayer view to the FrameLayout, and set it to fill all the area available.
        this.fl_forUnity.addView(mUnityPlayer.getView(),
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        //Initialize the buttons from the XML file.
        this.bt_rotLeft = (Button) findViewById(R.id.bt_rotLeft);
        this.bt_rotRight = (Button) findViewById(R.id.bt_rotRight);
        this.bt_stop = (Button) findViewById(R.id.bt_stop);

        /*Send a brodcast a message to the 'Main Camera' game object, passing L, R according to the
        pressed buttons.*/
        this.bt_rotLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnityPlayer.UnitySendMessage("Main Camera", "ReceiveRotDir", "L");
            }
        });

        this.bt_rotRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnityPlayer.UnitySendMessage("Main Camera", "ReceiveRotDir", "R");
            }
        });

        this.bt_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnityPlayer.UnitySendMessage("Main Camera", "ReceiveRotDir", " ");
            }
        });

        mUnityPlayer.requestFocus();

        /**
         * Code for the general Activity functions/variables.
         */
        state1 = (RelativeLayout) findViewById(R.id.state1);
        state2 = (RelativeLayout) findViewById(R.id.state2);
        connectBle = (Button) findViewById(R.id.connect_ble);
        disconnectBle = (Button) findViewById(R.id.disconnect_ble);
        exerciseLayout = (RelativeLayout) findViewById(R.id.ex_layout);
        visualLayout = (RelativeLayout) findViewById(R.id.vis_layout);
        summaryLayout = (RelativeLayout) findViewById(R.id.med_layout);
        upperLeg = (Button) findViewById(R.id.upper_leg);
        lowerLeg = (Button) findViewById(R.id.lower_leg);
        exerciseView = (Button) findViewById(R.id.ex_view);
        visualView = (Button) findViewById(R.id.vis_view);
        summaryView = (Button) findViewById(R.id.med_view);
        linegraph = (LineGraph)findViewById(R.id.graph);
        //reconnectBle = (Button) findViewById(R.id.reconnect_ble);
        statusText = (TextView) findViewById(R.id.status);
        resultText = (TextView) findViewById(R.id.result);
        resultVal = (TextView) findViewById(R.id.result_val);
        //dataAngle1Text = (TextView) findViewById(R.id.data_angle1);
        //dataAngle2Text = (TextView) findViewById(R.id.data_angle2);
        //dataAngleText = (TextView) findViewById(R.id.data_angle);
        scanStatus = (TextView) findViewById(R.id.scan_status);
        state1.setVisibility(View.VISIBLE);
        state2.setVisibility(View.INVISIBLE);
        connectBle.setVisibility(View.INVISIBLE);

        upperLeg.append("Select device");
        upperLeg.setBackgroundColor(Color.parseColor("#33FFFF00"));
        summaryLayout.setVisibility(View.INVISIBLE);
        visualLayout.setVisibility(View.INVISIBLE);

        /**
         * Code for AWS Management functions (User Identity/DynamoDB).
         */
        identityManager = AWSMobileClient.defaultMobileClient()
                .getIdentityManager();
        identityManager.addSignInStateChangeListener(this);
        fetchUserIdentity();

        demoTable = DemoNoSQLTableFactory.instance(this.getApplicationContext()).getNoSQLTableByTableName(BUNDLE_ARGS_TABLE_TITLE);

        /**
         * Code for BLE Management functions (Initialization/Connection).
         */
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Log.w("BleActivity", "Location access not granted!");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_RESPONSE);
        }

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        /**
         * Code for starting connection to multiple BLE devices. (Start Exercise)
         * if user correctly selects 2 devices, then activity proceeds to the exercise layout(state2)
         * if user incorrectly selects 0 or 1 device, then a toast is displayed.
         * if user incorrectly selects more than 2 devices, then a toast is displayed. (currently requires app restart)
         */
        connectBle.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                v.startAnimation(buttonClick);
                if (deviceCount == 2) {
                    scanStatus.setVisibility(View.INVISIBLE);
                    spinner.setVisibility(View.GONE);
                    statSpinner.setVisibility(View.VISIBLE);
                    state1.setVisibility(View.INVISIBLE);
                    state1.setClickable(false);
                    state2.setVisibility(View.VISIBLE);
                    state2.setClickable(true);
//                reconnectBle.setVisibility(View.INVISIBLE);
//                reconnectBle.setClickable(false);
                    Intent gattServiceIntent = new Intent(ScanActivity.this, BleService.class);
                    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
                    Log.w(TAG, "START CONNECTION");
                } else {
                    if (deviceCount < 2) {
                        Toast.makeText(ScanActivity.this, "Select 2 devices to connect.", Toast.LENGTH_LONG).show();
                    }
                    if (deviceCount > 2) {
                        Toast.makeText(ScanActivity.this, "Connected to more than 2 devices.", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ScanActivity.this, "Clearning queue", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        /**
         * Code for disconnecting from multiple BLE devices. (End Exercise)
         * If exercise was conducted, then proceed with data analysis and storage of the following analysis
         * in AWS DynamoDB.
         */
        disconnectBle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnectBle.setVisibility(View.INVISIBLE);
                disconnectBle.setClickable(false);
                //unityPlayer.setVisibility(View.INVISIBLE);
//                reconnectBle.setVisibility(View.VISIBLE);
//                reconnectBle.setClickable(true);
                Log.w(TAG, "Disconnecting from BLE devices");
                mBleService.disconnectServices();
                executeQuit = true;

                /** Statement to check whether the exercise was started. */
                if (mStreaming == true) {
                    Calendar end = Calendar.getInstance();
                    endMin= end.get(Calendar.MINUTE);
                    endSec = end.get(Calendar.SECOND);

                    duration = (int)Math.sqrt(Math.pow(endMin - startMin,2)) + " m "
                            + (int)Math.sqrt(Math.pow(endSec - startSec,2)) + " s";

                    /** Split data into extension and flexion. */
                    dataSplitFunction(angleDataSet);

                    /** Total Range of Motion. */
                    angleMax = Collections.max(angleDataSet);

                    /** Extension Data Computations. */
                    ext_min = Collections.min(extDataSet);
                    ext_max = Collections.max(extDataSet);
                    for (int i = 0; i < extDataSet.size(); i++) {
                        ext_mean = ext_mean + extDataSet.get(i);
                    }
                    ext_mean = ext_mean/extDataSet.size();
                    ext_var = varianceFunction(extDataSet);
                    ext_std = Math.sqrt(ext_var);

                    /** Flexion Data Computations. */
                    flx_min = Collections.min(flexDataSet);
                    flx_max = Collections.max(flexDataSet);
                    for (int i = 0; i < flexDataSet.size(); i++) {
                        flx_mean = flx_mean + flexDataSet.get(i);
                    }
                    flx_mean = flx_mean/flexDataSet.size();
                    flx_var = varianceFunction(flexDataSet);
                    flx_std = Math.sqrt(flx_var);

                    Log.w(TAG, "Start time: " + startMin + ":" + startSec);
                    Log.w(TAG, "End time: " + endMin + ":" + endSec);

                    /**
                     * CALCULATION AND RESULTS
                     * duration: duration of exercise in minutes and seconds
                     * range of motion: total range of motion calculated by max angle
                     * extension: movement of leg from 90 degrees to 0 degrees
                     * flexion: movement of leg from 0 degrees to 90 degrees
                     * minimum extension/flexion: minimum point of extension (usually 90)
                     * maximum extension/flexion: maximum point of extension (about 0)
                     * mean extension/flexion: mean value of extension
                     * variance of extension/flexion: variation calculation of extension
                     * std of extension/flexion: standard deviation calculation of extension
                     */
                    resultText.append("Duration:" + "\n");
                    resultText.append("Range of motion:" + "\n");
                    resultText.append("Minimum extension:" + "\n");
                    resultText.append("Maximum extension:" + "\n");
                    resultText.append("Mean extension:" +"\n");
                    resultText.append("Variance of extension:" + "\n");
                    resultText.append("Standard deviation of extension:" + "\n");
                    resultText.append("Minimum flexion:" + "\n");
                    resultText.append("Maximum flexion:" + "\n");
                    resultText.append("Mean flexion:" +"\n");
                    resultText.append("Variance of flexion:" + "\n");
                    resultText.append("Standard deviation of flexion:" + "\n");

                    resultVal.setText(duration + "\n");
                    resultVal.append(String.format("%.4f", angleMax) + "\n");
                    resultVal.append(String.format("%.4f", ext_min) + "\n");
                    resultVal.append(String.format("%.4f", ext_max) + "\n");
                    resultVal.append(String.format("%.4f", ext_mean) +"\n");
                    resultVal.append(String.format("%.4f", ext_var) + "\n");
                    resultVal.append(String.format("%.4f", ext_std) + "\n");
                    resultVal.append(String.format("%.4f", flx_min) + "\n");
                    resultVal.append(String.format("%.4f", flx_max) + "\n");
                    resultVal.append(String.format("%.4f", flx_mean) +"\n");
                    resultVal.append(String.format("%.4f", flx_var) + "\n");
                    resultVal.append(String.format("%.4f", flx_std) + "\n");


                    //resultText.setText("Number of data collected: " + angleDataSet.size() + "\n");
                    //resultText.append("Extension data set size: " + extDataSet.size() + "\n");
                    //resultText.append("Flexion data set size: " + flexDataSet.size() + "\n");

//                resultText.append("\n" + "Extension Data" + "\n");
//                for (int i = 0; i < extDataSet.size(); i++) {
//                    resultText.append(String.format("%.4f", extDataSet.get(i)) + "\n");
//                }
//                resultText.append("\n" + "Flexion Data" +"\n");
//                for (int i = 0; i < flexDataSet.size(); i++) {
//                    resultText.append(String.format("%.4f", flexDataSet.get(i)) + "\n");
//                }

                    /** Getting current date and time for use in primary key generation. */
                    Calendar cal = Calendar.getInstance();
                    currentDay = cal.get(Calendar.DAY_OF_MONTH);
                    SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                    currentMonth = month_date.format(cal.getTime());
                    currentYear = cal.get(Calendar.YEAR);
                    currentHr = cal.get(Calendar.HOUR_OF_DAY);
                    currentMin = cal.get(Calendar.MINUTE);

                    /** Thread for storing analyzed data on AWS DynamoDB with table name: exowear-mobilehub-805305975-ExoWearDB. */
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                primaryKey = currentDay + currentMonth + currentYear + " " + username + " " + currentHr + ":" + currentMin;
                                demoTable.insertData(primaryKey,  duration, String.format("%.4f", angleMax), String.format("%.4f", ext_min),
                                        String.format("%.4f", ext_max), String.format("%.4f", ext_mean), String.format("%.4f", ext_var),
                                        String.format("%.4f", ext_std), String.format("%.4f", flx_min), String.format("%.4f", flx_max),
                                        String.format("%.4f", flx_mean), String.format("%.4f", flx_var), String.format("%.4f", flx_std));
                            } catch (final AmazonClientException ex) {
                                // The insertSampleData call already logs the error, so we only need to
                                // show the error dialog to the user at this point.
                                return;
                            }
                        }
                    }).start();
                }
            }
        });

        exerciseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciseLayout.setVisibility(View.VISIBLE);
                exerciseView.setBackgroundColor(Color.parseColor("#26ffffff"));
                summaryLayout.setVisibility(View.INVISIBLE);
                summaryView.setBackgroundColor(Color.TRANSPARENT);
                visualLayout.setVisibility(View.INVISIBLE);
                visualView.setBackgroundColor(Color.TRANSPARENT);
            }
        });

        visualView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visualLayout.setVisibility(View.VISIBLE);
                visualView.setBackgroundColor(Color.parseColor("#26ffffff"));
                exerciseLayout.setVisibility(View.INVISIBLE);
                exerciseView.setBackgroundColor(Color.TRANSPARENT);
                summaryLayout.setVisibility(View.INVISIBLE);
                summaryView.setBackgroundColor(Color.TRANSPARENT);
            }
        });

        summaryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                summaryLayout.setVisibility(View.VISIBLE);
                summaryView.setBackgroundColor(Color.parseColor("#26ffffff"));
                exerciseLayout.setVisibility(View.INVISIBLE);
                exerciseView.setBackgroundColor(Color.TRANSPARENT);
                visualLayout.setVisibility(View.INVISIBLE);
                visualView.setBackgroundColor(Color.TRANSPARENT);

            }
        });

//        reconnectBle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                state1.setVisibility(View.VISIBLE);
//                state1.setClickable(true);
//                state2.setVisibility(View.INVISIBLE);
//                state2.setClickable(false);
//            }
//        });

        /**
         * Code for SQLite database functions.
         */
        exoweardb = new DBHelper(this);

        /**
         * Code for animated textview ex. Scanning -> Scanning. -> Scanning.. -> Scanning...
         */
        scanStatus.setText("Scanning");
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (scan_counter == 3) {
                                    scanStatus.setText("Scanning");
                                    scan_counter = 0;
                                }
                                else {
                                    scanStatus.append(".");
                                    scan_counter++;
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUnityPlayer.resume();
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);
        scanLeDevice(true);

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        mUnityPlayer.pause();
        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        mUnityPlayer.quit();
        super.onDestroy();
        unbindService(mServiceConnection);
        mBleService = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);

        v.setBackgroundColor(Color.parseColor("#3301FF01"));
        v.setEnabled(false);
        v.setOnClickListener(null);
        if (device == null) return;
        else {
            Intent intent = new Intent(this, BleService.class);
            intent.putExtra("DEVICE", (Parcelable) device);
            this.startService(intent);
        }
        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        deviceCount++;
        if (deviceCount == 1) {
            upperLeg.setText("Thigh" + "\n" + "device selected:" + "\n" + device.getName());
            upperLeg.setBackgroundColor(Color.parseColor("#3301FF01"));
            lowerLeg.append(("Select device"));
            lowerLeg.setBackgroundColor(Color.parseColor("#33FFFF00"));
        }
        if (deviceCount == 2) {
            lowerLeg.setText("Shin" + "\n" + "device selected:" + "\n" + device.getName());
            lowerLeg.setBackgroundColor(Color.parseColor("#3301FF01"));
            l.setClickable(false);
            connectBle.setVisibility(View.VISIBLE);
        }
        Log.w(TAG, device.getName());
        mLeDeviceListAdapter.notifyDataSetChanged();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = ScanActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
                mLeDeviceListAdapter.notifyDataSetChanged();
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (device.getName().startsWith("ExoWear")) {
                                    mLeDeviceListAdapter.addDevice(device);
                                    mLeDeviceListAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {}
                        }
                    });
                }
            };

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBleService = ((BleService.LocalBinder) service).getService();
            if (!mBleService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            Log.w(TAG, "SERVICE CONNECTION REACHED");
            mBleService.initConnection();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBleService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BleService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                Log.w(TAG, "CONNECTED");
                statusText.setText("Connecting");
                statusText.setTextColor(Color.parseColor("#FFA500"));
                statSpinner.getIndeterminateDrawable().setColorFilter(0xFFFFA500,
                        android.graphics.PorterDuff.Mode.MULTIPLY);
                registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
                if (mBleService != null) {
                    int cqueue = mBleService.connectionQueueSize();
                    int dqueue = mBleService.discoveryQueueSize();
                    Log.w(TAG, String.valueOf(cqueue));
                    Log.w(TAG, String.valueOf(dqueue));
                    if (dqueue == deviceCount) {
                        mBleService.initServiceDiscovery();
                    }
                }
            } else if (BleService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                Log.w(TAG, "DISCONNECTED");
                statSpinner.setVisibility(View.GONE);
                statusText.setText("Disconnected");
                statusText.setTextColor(Color.parseColor("#ff0000"));
                statusText.setTextSize(23);
            } else if (BleService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                Log.w(TAG, "GATT SERVICES DISCOVERED");
                statusText.setText("Discovered");
                statusText.setTextColor(Color.parseColor("#FFFF00"));
                statSpinner.getIndeterminateDrawable().setColorFilter(0xFFFFFF00,
                        android.graphics.PorterDuff.Mode.MULTIPLY);
                try {
                    mBleService.serviceDisplay();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }

                try {
                    Thread.sleep(5000);
                    Log.w(TAG, "service discovered: " + String.valueOf(mBleService.discoveredQueueSize()));
                    Log.w(TAG, "char notify: " + String.valueOf(mBleService.notifyQueueSize()));
                } catch (InterruptedException e) {
                }

                mBleService.broadcastNotify();

            } else if (BleService.ACTION_DATA_AVAILABLE.equals(action)) {
                if (executeQuit != true) {
                    statusText.setText("Streaming");
                    statusText.setTextColor(Color.parseColor("#00FF00"));
                    statSpinner.getIndeterminateDrawable().setColorFilter(0xFF00FF00,
                            android.graphics.PorterDuff.Mode.MULTIPLY);
                    Date d = new Date();
                    mStreaming = true;

                    String data = intent.getStringExtra(BleService.EXTRA_DATA);

                    li = data.split(":");
                    float w = Float.parseFloat(li[0]);
                    float x = Float.parseFloat(li[1]);
                    float y = Float.parseFloat(li[2]);
                    float z = Float.parseFloat(li[3]);

                    String device_name = li[4];

                    Quant4d qua = new Quant4d(w, x, y, z);

                    if (device_name.equals("ExoWear12")) {
                        String sendData = "" + qua.getW() + ',' + qua.getY() + ',' + qua.getZ() + ',' + qua.getX();
                        //Log.w(TAG, "Quaternion Data:       W: " +  qua.getW() + " X: " +  qua.getX() + " Y: " +  qua.getY() + " Z: "+ qua.getZ());
                        try{
                            UnityPlayer.UnitySendMessage("Player", "ReceiveRotDir", sendData);}
                        catch (Exception e ){
                            Log.w(TAG, "PLayer 1 Rotation Code has problem................");
                        }
                    }
                    else if (device_name.equals("ExoWear6")) {
                        //Log.w(TAG, "CONNECT TO SECOND ONE");
                        String sendData = "" + qua.getW() + ',' + qua.getY() + ',' + qua.getZ() + ',' + qua.getX();
                        //Log.w(TAG, "Quaternion Data:       W: " +  qua.getW() + " X: " +  qua.getX() + " Y: " +  qua.getY() + " Z: "+ qua.getZ());
                        try{
                            UnityPlayer.UnitySendMessage("Player2", "ReceiveRotDir", sendData);
                        }
                        catch(Exception e ){ Log.w(TAG, "PLayer 2 Rotation Code has problem................");}

                    }
                    else{
                        Log.w(TAG, "TRYING TO CONNECT EITHER ONE TO WAIT OR PROBLEMS COME OUT ");
                    }

                    axisAngle ax = qua.transQToAx();
                    ax.setAngle(Math.toDegrees(ax.getAngle()));

                    float ang = (float) ax.getAngle();
                    float mx = (float) ax.getX();
                    float my = (float) ax.getY();
                    float mz = (float) ax.getZ();

                    //Log.w(TAG, device_name + "  W = " + w + " X = " + x + ", Y = " + y + " Z = " + z);

                    if (device_counter == 0 && device_name != null) {
                        device1 = device_name;
                        device_counter++;
                        //Log.w(TAG, "Connect to the first one !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    } else if (device_counter == 1 && device_name != null && !device_name.equals(device1)) {
                        device2 = device_name;
                        device_counter++;
                        //Log.w(TAG, "Connect to the second one !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    } else if (device_counter >= 2 && device_name != null && device_name.equals(device1)) {

                        if (start_record1) {
                            start_record1 = false;
                            start_time1 = System.currentTimeMillis();
                            Log.w(TAG, "Waiting for 10 seconds to elapse.");
                        } else if (System.currentTimeMillis() - start_time1 > 10000) { // wait for 15 seconds to go by
                            /* Read the first 30 quaternions (sensor assumed to remain still) and compute their average
                            /* Read the first 30 quaternions (sensor assumed to remain still) and compute their average
                                to get a good starting quaternion.
                             */
                            if (calib_msg_count1 < 30) {
                                calib_points1.add(qua);
                                calib_msg_count1++;
                            } else if (calib_msg_count1 == 30) {
                                /* Compute average of the first 30 points */
                                double sumx = 0;
                                double sumy = 0;
                                double sumz = 0;
                                double sumw = 0;
                                for (Quant4d q : calib_points1) {
                                    sumw = sumw + q.getW();
                                    sumx = sumx + q.getX();
                                    sumy = sumy + q.getY();
                                    sumz = sumz + q.getZ();

                                }
                                double avgw = sumw / calib_points1.size();
                                double avgx = sumx / calib_points1.size();
                                double avgy = sumy / calib_points1.size();
                                double avgz = sumz / calib_points1.size();

                                start1 = new Quant4d(avgw, avgx, avgy, avgz);
                                for (Quant4d q : calib_points1) {
                                    Log.w(TAG, "######## " + q + " ########");
                                }
                                Log.w(TAG, "******************* " + avgx + " " + avgy + " " + avgz + " *******************");
                                calib_msg_count1++;
                            } else {
                                /* For the new current quaternion, compute the quaternion that rotates the
                                    start2 quaternion to this new quaternion.
                                    start2: q0
                                    this quat:  qt
                                    Find q1 s.t.     qt = q1*q0 --> q1 = qt*q0^(-1)
                                 */

                                if (valCounter1 < AVERAGE_RATE) {
                                    //tempDataSet.add(angle);
                                    quatDataSet1.add(qua);
                                    valCounter1++;
                                }
                                if (valCounter1 == AVERAGE_RATE) {
                                    double sumx = 0;
                                    double sumy = 0;
                                    double sumz = 0;
                                    double sumw = 0;
                                    for (Quant4d q : quatDataSet1) {
                                        sumw = sumw + q.getW();
                                        sumx = sumx + q.getX();
                                        sumy = sumy + q.getY();
                                        sumz = sumz + q.getZ();

                                    }
                                    double avgw = sumw / calib_points1.size();
                                    double avgx = sumx / calib_points1.size();
                                    double avgy = sumy / calib_points1.size();
                                    double avgz = sumz / calib_points1.size();
                                    Quant4d q = new Quant4d(avgw, avgx, avgy, avgz);

                                    Quant4d q1 = Quant4d.hamiltonian(q, start1.conjugate());
                                /* Get start2 vector based on which axis we are rotating the sensor around. */
                                    vector v0;
                                    if (spinAxis == 'x') {
                                        v0 = new vector(0.0, 0.0, 1.0);
                                    } else if (spinAxis == 'y') {
                                        v0 = new vector(1.0, 0.0, 0.0);
                                    } else {
                                        v0 = new vector(0.0, 1.0, 0.0);
                                    }
                                    Quant4d tempq = Quant4d.hamiltonian(q1, vector.vtoq(v0));
                                    Quant4d newv = Quant4d.hamiltonian(tempq, q1.conjugate());
                                    Quant4d v2;
                                    vector v2v = Quant4d.qtov(newv);
                                    double angle = FastMath.acos(v0.dot(v2v) / (v0.mag() * v2v.mag())) * 180.0 / FastMath.PI;
                                    angle1 = angle;
                                    quatDataSet1.clear();
                                    valCounter1 = 0;
                                }
                            }
                        }

                    } else if (device_counter >= 2 && device_name != null && device_name.equals(device2)) {
                        /* When the connection is first started, wait 15 seconds to allow sensor
                            to stabilize if it just turned on.
                         */
                        if (start_record2) {
                            start_record2 = false;
                            start_time2 = System.currentTimeMillis();
                            Log.w(TAG, "Waiting for 10 seconds to elapse.");
                        } else if (System.currentTimeMillis() - start_time2 > 10000) { // wait for 15 seconds to go by
                            /* Read the first 30 quaternions (sensor assumed to remain still) and compute their average
                            /* Read the first 30 quaternions (sensor assumed to remain still) and compute their average
                                to get a good starting quaternion.
                             */
                            if (calib_msg_count2 < 30) {
                                calib_points2.add(qua);
                                calib_msg_count2++;
                            } else if (calib_msg_count2 == 30) {
                                /* Compute average of the first 30 points */
                                double sumx = 0;
                                double sumy = 0;
                                double sumz = 0;
                                double sumw = 0;
                                for (Quant4d q : calib_points2) {
                                    sumw = sumw + q.getW();
                                    sumx = sumx + q.getX();
                                    sumy = sumy + q.getY();
                                    sumz = sumz + q.getZ();

                                }
                                double avgw = sumw / calib_points2.size();
                                double avgx = sumx / calib_points2.size();
                                double avgy = sumy / calib_points2.size();
                                double avgz = sumz / calib_points2.size();

                                start2 = new Quant4d(avgw, avgx, avgy, avgz);
                                for (Quant4d q : calib_points2) {
                                    Log.w(TAG, "######## " + q + " ########");
                                }
                                Log.w(TAG, "******************* " + avgx + " " + avgy + " " + avgz + " *******************");
                                calibrate = false;
                                calib_msg_count2++;
                            } else {
                                /* For the new current quaternion, compute the quaternion that rotates the
                                    start2 quaternion to this new quaternion.
                                    start2: q0
                                    this quat:  qt
                                    Find q1 s.t.     qt = q1*q0 --> q1 = qt*q0^(-1)
                                 */
//                                Quant4d q1 = Quant4d.hamiltonian(qua, start2.conjugate());
//                                /* Get start2 vector based on which axis we are rotating the sensor around. */
//                                vector v0;
//                                if (spinAxis == 'x') {
//                                    v0 = new vector(0.0, 0.0, 1.0);
//                                } else if (spinAxis == 'y') {
//                                    v0 = new vector(1.0, 0.0, 0.0);
//                                } else {
//                                    v0 = new vector(0.0, 1.0, 0.0);
//                                }
//                                /* Apply quaternion q1 to the starting vector v0
//                                    v0' = q1*v0*q1^(-1)
//                                 */
//                                Quant4d tempq = Quant4d.hamiltonian(q1,vector.vtoq(v0));
//                                Quant4d newv = Quant4d.hamiltonian(tempq, q1.conjugate());
//                                Quant4d v2;
//                                /* Based on the axis of rotation, ignore one axis of the final vector
//                                    in order to isolate the angle that we want to measure.
//                                 */
//                                if (spinAxis == 'x') {
//                                    v2 = new Quant4d(newv.getW(), 0.0, newv.getY(), newv.getZ());
//                                } else if (spinAxis == 'y') {
//                                    v2 = new Quant4d(newv.getW(), newv.getX(), 0.0, newv.getZ());
//                                } else {
//                                    v2 = new Quant4d(newv.getW(), newv.getX(), newv.getY(), 0.0);
//                                }
//                                vector v2v = Quant4d.qtov(newv);
//                                /* Calculate the angle between the initial vector v0 and the new vector v2
//                                    theta = arccos(v0.v2/(|v0||v2|)) -->
//                                 */
//                                double angle = FastMath.acos(v0.dot(v2v)/(v0.mag()*v2v.mag()))*180.0/FastMath.PI;
//                                //if (printcount++ % printrate == 0) {
//                                //Log.w(TAG, " Angle difference " + angle);
//                                //}

                                if (valCounter2 < AVERAGE_RATE) {
                                    //tempDataSet.add(angle);
                                    quatDataSet2.add(qua);
                                    valCounter2++;
                                }
                                if (valCounter2 == AVERAGE_RATE) {
                                    double sumx = 0;
                                    double sumy = 0;
                                    double sumz = 0;
                                    double sumw = 0;
                                    for (Quant4d q : quatDataSet2) {
                                        sumw = sumw + q.getW();
                                        sumx = sumx + q.getX();
                                        sumy = sumy + q.getY();
                                        sumz = sumz + q.getZ();

                                    }
                                    double avgw = sumw / calib_points2.size();
                                    double avgx = sumx / calib_points2.size();
                                    double avgy = sumy / calib_points2.size();
                                    double avgz = sumz / calib_points2.size();
                                    Quant4d q = new Quant4d(avgw, avgx, avgy, avgz);

                                    Quant4d q1 = Quant4d.hamiltonian(q, start2.conjugate());
                                    /* Get start2 vector based on which axis we are rotating the sensor around. */
                                    vector v0;
                                    if (spinAxis == 'x') {
                                        v0 = new vector(0.0, 0.0, 1.0);
                                    } else if (spinAxis == 'y') {
                                        v0 = new vector(1.0, 0.0, 0.0);
                                    } else {
                                        v0 = new vector(0.0, 1.0, 0.0);
                                    }
                                    Quant4d tempq = Quant4d.hamiltonian(q1, vector.vtoq(v0));
                                    Quant4d newv = Quant4d.hamiltonian(tempq, q1.conjugate());
                                    Quant4d v2;
                                    vector v2v = Quant4d.qtov(newv);
                                    double angle = FastMath.acos(v0.dot(v2v) / (v0.mag() * v2v.mag())) * 180.0 / FastMath.PI;
                                    angle2 = angle;
                                    quatDataSet2.clear();
                                    valCounter2 = 0;
                                    Log.w(TAG, "Angle Difference " + device_name + ": " + angle);
                                }
                            }
                        }
                    } else {
                        statusText.setText("Error code 941: angle data calculation");
                        Log.w(TAG, "!!! Somethings is wrong !!!");
                    }
                    double kneeAngle = 180 - (90.0 - angle1 + angle2);
                    Log.w(TAG, "Calibrating...");
                    Log.w(TAG, "Angle 1:" + angle1);
                    Log.w(TAG, "Angle 2:" + angle2);
                    //dataAngle1Text.setText(String.format("%.4f", angle1));
                    //dataAngle2Text.setText(String.format("%.4f", angle2));
                    Log.w(TAG, "Knee angle:" + String.valueOf(kneeAngle));
                    statusText.setText(String.format("%.4f", kneeAngle));
                    statusText.setTextSize(26);
                    //dataAngleText.setText(String.format("%.4f", kneeAngle));

                    if (calibrate != true) {
                        statusText.setText("Start");
                        statSpinner.setVisibility(View.GONE);
                        statusText.setTextColor(Color.parseColor("#00FFFF"));

                        angleDataSet.add(kneeAngle);
                        statusText.setText(String.format("%.4f", kneeAngle));
                        statusText.setTextSize(26);
                        //dataAngleText.setText(String.format("%.4f", kneeAngle));

                        if (startCount == true) {
                            Calendar start = Calendar.getInstance();
                            startMin= start.get(Calendar.MINUTE);
                            startSec = start.get(Calendar.SECOND);
                        }
                        startCount = false;
                    } else {
                        statusText.setText("Ready");
                        statusText.setTextColor(Color.parseColor("#FF00FF"));
                        statSpinner.getIndeterminateDrawable().setColorFilter(0xFFFF00FF,
                                android.graphics.PorterDuff.Mode.MULTIPLY);
                    }
                }

            }
        }
    };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BleService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BleService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
    /*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }

    /**
     * Fetches the user identity safely on the background thread.  It may make a network call.
     */
    private void fetchUserIdentity() {

        // Pre-fetched to avoid race condition where fragment is no longer active.
        final String unknownUserIdentityText =
                getString(R.string.identity_demo_unknown_identity_text);

        AWSMobileClient.defaultMobileClient()
                .getIdentityManager()
                .getUserID(new IdentityManager.IdentityHandler() {

                    @Override
                    public void handleIdentityID(String identityId) {

                        // We have successfully retrieved the user's identity. You can use the
                        // user identity value to uniquely identify the user. For demonstration
                        // purposes here, we will display the value in a text view.

                        //userIdTextView.setText(identityId);
                        userid = identityId;
                        Log.w(TAG, userid);

                        if (identityManager.isUserSignedIn()) {
                            username = identityManager.getUserName();
                            Log.w(TAG, username);
                            //userNameTextView.setText(identityManager.getUserName());
                        }
                    }

                    @Override
                    public void handleError(Exception exception) {
                        // We failed to retrieve the user's identity. Set unknown user identifier
                        // in text view.
                    }
                });
    }

    @Override
    public void onUserSignedIn() {
        // Update the user identity to account for the user signing in.
        fetchUserIdentity();
    }

    @Override
    public void onUserSignedOut() {
        // Update the user identity to account for the user signing out.
        fetchUserIdentity();
    }

    /**
     * ALGORITHMS AND DATA ANALYTICS. PROPERTY OF KEVIN LEE OF EXOWEAR LLC.
     */

    public void dataSplitFunction(ArrayList<Double> angleData) {
        Log.w(TAG, "Data-split Function");
        try {
            max = Collections.max(angleData);
            for (int i = 0; i < angleData.size(); i++) {
                if (angleData.get(i) == max) {
                    index = i;
                    break;
                }
            }

            //Data for extension
            for (int i = 0; i <= index; i++) {
                extDataSet.add(angleData.get(i));
            }

            //Data for flexion
            for (int i = index; i < angleData.size(); i++) {
                flexDataSet.add(angleData.get(i));
            }
            Log.w(TAG, "Success: data-split function");
            Log.w(TAG, "Report" + "\n" + "extension data size: " + extDataSet.size() + "\n" + "flexion data size: " +
                    flexDataSet.size());

        } catch (Exception e) {
            Log.w(TAG, "Error code 846: data-split function");
        }
    }

    public double varianceFunction(ArrayList<Double> angleData) {
        Log.w(TAG, "Variance Function");
        //Calculating the average (mean)
        for (int i = 0; i < angleData.size(); i++) {
            mean = mean + angleData.get(i);
        }
        mean = mean/angleData.size();

        //Calculating the difference from the mean and squaring the value
        for (int i = 0; i < angleData.size(); i++) {
            varDataSet.add(Math.pow(angleData.get(i)-mean,2));
        }

        //Final calculations to obtain variation
        for (int i = 0; i < varDataSet.size(); i++) {
            var = var + varDataSet.get(i);
        }
        var = var/varDataSet.size();

        return var;
    }
}