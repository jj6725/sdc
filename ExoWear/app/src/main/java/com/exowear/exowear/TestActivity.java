package com.exowear.exowear;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.amazonaws.AmazonClientException;
import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.util.ThreadUtils;
import com.exowear.exowear.demo.nosql.DemoNoSQLTableBase;
import com.exowear.exowear.demo.nosql.DemoNoSQLTableFactory;
import com.exowear.exowear.demo.nosql.DynamoDBUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class TestActivity extends AppCompatActivity implements IdentityManager.SignInStateChangeListener {
    private static final String LOG_TAG = TestActivity.class.getSimpleName();

    /** Bundle key for retrieving the table name from the fragment's arguments. */
    public static final String BUNDLE_ARGS_TABLE_TITLE = "ExoWearDB";

    /** The NoSQL Table demo operations will be run against. */
    private DemoNoSQLTableBase demoTable;

    /** The identity manager used to keep track of the current user account. */
    private IdentityManager identityManager;

    /** Text view for showing the user identity. */
    private TextView userIdTextView;

    /** Text view for showing the user name. */
    private TextView userNameTextView;

    public Button addUserData;
    private int count = 0;
    private String userid;
    private String keyuserid;
    private String quat1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        userIdTextView = (TextView) findViewById(R.id.userid);
        userNameTextView = (TextView) findViewById(R.id.username);
        addUserData = (Button) findViewById(R.id.add_data);

        identityManager = AWSMobileClient.defaultMobileClient()
                .getIdentityManager();
        identityManager.addSignInStateChangeListener(this);
        fetchUserIdentity();

        demoTable = DemoNoSQLTableFactory.instance(this.getApplicationContext()).getNoSQLTableByTableName(BUNDLE_ARGS_TABLE_TITLE);

//        addUserData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            keyuserid = userid + "-" + count;
//                            quat1 = "0.1256734,0.2123512,0.3245234,0.4245234";
//                            //Log.w(LOG_TAG, demoTable.getTableName());
//                            demoTable.insertData(keyuserid, quat1);
//                        } catch (final AmazonClientException ex) {
//                            // The insertSampleData call already logs the error, so we only need to
//                            // show the error dialog to the user at this point.
//                            return;
//                        }
//                    }
//                }).start();
//                count++;
//            }
//        });
    }

    /**
     * Fetches the user identity safely on the background thread.  It may make a network call.
     */
    private void fetchUserIdentity() {

        // Pre-fetched to avoid race condition where fragment is no longer active.
        final String unknownUserIdentityText =
                getString(R.string.identity_demo_unknown_identity_text);

        AWSMobileClient.defaultMobileClient()
            .getIdentityManager()
            .getUserID(new IdentityManager.IdentityHandler() {

                @Override
                public void handleIdentityID(String identityId) {

                    // We have successfully retrieved the user's identity. You can use the
                    // user identity value to uniquely identify the user. For demonstration
                    // purposes here, we will display the value in a text view.

                    userIdTextView.setText(identityId);
                    userid = identityId;

                    if (identityManager.isUserSignedIn()) {
                        userNameTextView.setText(identityManager.getUserName());
                    }
                }

                @Override
                public void handleError(Exception exception) {

                    // We failed to retrieve the user's identity. Set unknown user identifier
                    // in text view.

                    userIdTextView.setText(unknownUserIdentityText);

                    //final Context context = getActivity();

//                    if (context != null && isAdded()) {
//                        new AlertDialog.Builder(getActivity())
//                                .setTitle(R.string.identity_demo_error_dialog_title)
//                                .setMessage(getString(R.string.identity_demo_error_message_failed_get_identity)
//                                        + exception.getMessage())
//                                .setNegativeButton(R.string.identity_demo_dialog_dismiss_text, null)
//                                .create()
//                                .show();
//                    }
                }
            });
    }

    @Override
    public void onUserSignedIn() {
        // Update the user identity to account for the user signing in.
        fetchUserIdentity();
    }

    @Override
    public void onUserSignedOut() {
        // Update the user identity to account for the user signing out.
        fetchUserIdentity();
    }
}
