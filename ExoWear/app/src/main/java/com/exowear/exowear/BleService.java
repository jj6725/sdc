package com.exowear.exowear;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Quat4d;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BleService extends Service {
    private final static String TAG = BleService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    public BluetoothGattDescriptor descriptor;
    public BluetoothGattCharacteristic mcharacteristic;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    public final static UUID UUID_QUATERNION_MEASUREMENT =
            UUID.fromString(GattAttributes.QUATERNION_MEASUREMENT);

    public ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics
            = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    public BluetoothGattCharacteristic mCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private Thread connectionThread;
    private Thread serviceDiscoveryThread;
    private Thread servicesThread;
    private Thread broadcastThread;
    private Thread disconnectThread;

    private Queue<BluetoothDevice> connectionQueue = new LinkedList<BluetoothDevice>();
    private Queue<BluetoothGatt> serviceDiscoveryQueue = new LinkedList<BluetoothGatt>(); //Queue of GATT
    private Queue<BluetoothGatt> serviceDiscoveredQueue = new LinkedList<BluetoothGatt>(); //Queue of GATT
    private Queue<BluetoothGattCharacteristic> characteristicReadQueue = new LinkedList<BluetoothGattCharacteristic>(); //Queue of characteristics
    private Queue<BluetoothGattCharacteristic> characteristicNotifyQueue = new LinkedList<BluetoothGattCharacteristic>(); //Queue of characteristics
    private Queue<BluetoothGatt> characteristicBroadcastQueue = new LinkedList<BluetoothGatt>(); //Queue of GATT
    private Queue<BluetoothGatt> serviceDisconnectQueue = new LinkedList<BluetoothGatt>(); //Queue of GATT
    private Queue<BluetoothGatt> serviceCloseQueue = new LinkedList<BluetoothGatt>(); //Queue of GATT

    private BluetoothDevice bleDevice;
    public boolean enabled;
    private String inputStream;
    public char[] packet = new char[14];
    public double w;
    public double x;
    public double y;
    public double z;


    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    public final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            switch(status){
                case BluetoothGatt.GATT_SUCCESS:
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        intentAction = ACTION_GATT_CONNECTED;
                        broadcastUpdate(intentAction);
                        Log.w(TAG, "Connected to GATT server.");
                        serviceDiscoveryQueue.add(gatt);
                        serviceDiscoveredQueue.add(gatt);
                        characteristicBroadcastQueue.add(gatt);
                        serviceDisconnectQueue.add(gatt);
                    }else if(newState == BluetoothProfile.STATE_DISCONNECTED){
                        intentAction = ACTION_GATT_DISCONNECTED;
                        broadcastUpdate(intentAction);
                        Log.w(TAG, "Disconnected from GATT server.");
                    }
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            String intentAction;
            if (status == BluetoothGatt.GATT_SUCCESS) {
                intentAction = ACTION_GATT_SERVICES_DISCOVERED;
                broadcastUpdate(intentAction);
                Log.w(TAG, "Services discovered for " + gatt.getDevice().getName());
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic,
                                         int status) {
            Log.w(TAG, "Characteristics being read.");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            //Log.w(TAG, "Characteristics being changed. " + gatt.getDevice().getName());
            broadcastUpdate(ACTION_DATA_AVAILABLE, gatt, characteristic);
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        Context context =  this.getBaseContext();
        context.sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        int serialCount = 0;

        //Log.w(TAG, "BroadcastUpdate");

        byte[] packetBytes = characteristic.getValue();

        if (packetBytes != null && packetBytes.length > 0) {
            for (int i = 0; i < packetBytes.length; i++) {
                byte b = packetBytes[i];
                char sb = (char) (b & 0xFF);
                packet[serialCount++] = sb;
                //Log.w(TAG, String.valueOf(serialCount));

                if (serialCount == 14) {
                    w = ((packet[2] << 8) | packet[3]) / 16384.0f;
                    x = ((packet[4] << 8) | packet[5]) / 16384.0f;
                    y = ((packet[6] << 8) | packet[7]) / 16384.0f;
                    z = ((packet[8] << 8) | packet[9]) / 16384.0f;
                }
            }

            if (w >= 2) {
                w = -4 + w;
            }
            if (x >= 2) {
                x = -4 + x;
            }
            if (y >= 2) {
                y = -4 + y;
            }
            if (z >= 2) {
                z = -4 + z;
            }
            intent.putExtra(EXTRA_DATA, String.valueOf(w) + ":" + String.valueOf(x) + ":" + String.valueOf(y) + ":" + String.valueOf(z) + ":" + gatt.getDevice().getName());
            sendBroadcast(intent);
        }
    }

    public class LocalBinder extends Binder {
        BleService getService() {
            return BleService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        //close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            bleDevice = intent.getExtras().getParcelable("DEVICE");
            connectionQueue.add(bleDevice);
            Log.w(TAG, "Device added to queue: " + bleDevice.getName());
            Log.w(TAG, "Number of devices in queue: " + connectionQueue.size());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void clearQueue() {
        Log.w(TAG, "Clearing connectionQueue");
        connectionQueue.clear();
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     */
    public void initConnection() {
        Log.w(TAG,"initConnection");
        if(connectionThread == null){
            connectionThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    connectionLoop();
                    connectionThread.interrupt();
                    connectionThread = null;
                }
            });

            connectionThread.start();
        }
    }

    private void connectionLoop() {
        while(!connectionQueue.isEmpty()) {
            connectionQueue.poll().connectGatt(this, false, mGattCallback);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }

    public void initServiceDiscovery() {
        Log.w(TAG,"initServiceDiscovery");
        if(serviceDiscoveryThread == null) {
            serviceDiscoveryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    serviceDiscovery();
                    serviceDiscoveryThread.interrupt();
                    serviceDiscoveryThread = null;
                }
            });

            serviceDiscoveryThread.start();
        }
    }

    private void serviceDiscovery() {
        while(!serviceDiscoveryQueue.isEmpty()) {
            Log.w(TAG, "Discovery Thread");
            serviceDiscoveryQueue.poll().discoverServices();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e){}
        }
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect(BluetoothGatt gatt) {
        if (gatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        gatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
//    public void close(BluetoothGatt gatt) {
//        if (mBluetoothGatt == null) {
//            return;
//        }
//        mBluetoothGatt.close();
//        mBluetoothGatt = null;
//    }

    public void disconnectServices(){

        if(disconnectThread == null){
            disconnectThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    disconnectQueues();
                    disconnectThread.interrupt();
                    disconnectThread = null;
                }
            });
            disconnectThread.start();
        }

    }

    private void disconnectQueues() {
        while(!serviceDisconnectQueue.isEmpty()){
            Log.w(TAG, "Disconnecting services");
            disconnect(serviceDisconnectQueue.poll());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     */
    public void readCharacteristic(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || gatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        Log.w(TAG, gatt.getDevice().getName());
        gatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || gatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        gatt.setCharacteristicNotification(characteristic, enabled);
        Log.w(TAG, "setCharacteristicNotification");

        Log.w(TAG, characteristic.getUuid().toString());
        if (UUID_QUATERNION_MEASUREMENT.equals(characteristic.getUuid())) {
            Log.w(TAG, "UUID service match");
            descriptor = characteristic.getDescriptor(UUID.fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            if (descriptor != null) {
                Log.w(TAG, "Descriptor match");
            }
            else {
                Log.w(TAG, "Descriptor null");
            }
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);
        }
    }

    public void serviceDisplay(){
        Log.w(TAG, "start Service Display");
        if(servicesThread == null){
            servicesThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    serviceDisplayQueues();
                    servicesThread.interrupt();
                    servicesThread = null;
                }
            });
            servicesThread.start();
        }
    }

    private void serviceDisplayQueues() {

        while(!serviceDiscoveredQueue.isEmpty()){
            Log.w(TAG, "Service Display Thread");
            getSupportedGattServices(serviceDiscoveredQueue.poll());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }

    public void broadcastNotify(){
        if(broadcastThread == null){
            broadcastThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    broadcastNotifyQueues();
                    broadcastThread.interrupt();
                    broadcastThread = null;
                }
            });
            broadcastThread.start();
        }
    }

    private void broadcastNotifyQueues() {
        Log.w(TAG, "Broadcast Notify Thread");
        while(!characteristicBroadcastQueue.isEmpty() && !characteristicNotifyQueue.isEmpty()){
            setCharacteristicNotification(characteristicBroadcastQueue.poll(), characteristicNotifyQueue.poll(), true);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }

    public void getSupportedGattServices(BluetoothGatt gatt) {
        Log.w(TAG, "Initiate service display " + gatt.getDevice().getName());
        displayGattServices(gatt.getServices());
        try {
            Thread.sleep(4000);
            if (mGattCharacteristics != null) {
                if (mGattCharacteristics.get(3).get(1) != null) {
                    Log.w(TAG, "success");
                    mNotifyCharacteristic = mGattCharacteristics.get(3).get(1);
                    characteristicNotifyQueue.add(mNotifyCharacteristic);
                }
            }
        } catch (InterruptedException e) {}
    }

    private void displayGattServices(List<BluetoothGattService> gattServices) {
        Log.w(TAG, "displayGattServices");
        if (gattServices == null) {
            Log.w(TAG, "no services");
            return;
        }
        else {
            Log.w(TAG, "start discovering services");
            String uuid = null;
            String unknownServiceString = getResources().getString(R.string.unknown_service);
            String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
            ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
            ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                    = new ArrayList<ArrayList<HashMap<String, String>>>();
            mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

            // Loops through available GATT Services.
            for (BluetoothGattService gattService : gattServices) {
                HashMap<String, String> currentServiceData = new HashMap<String, String>();
                uuid = gattService.getUuid().toString();
                currentServiceData.put(
                        LIST_NAME, GattAttributes.lookup(uuid, unknownServiceString));
                currentServiceData.put(LIST_UUID, uuid);
                gattServiceData.add(currentServiceData);

                ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                        new ArrayList<HashMap<String, String>>();
                List<BluetoothGattCharacteristic> gattCharacteristics =
                        gattService.getCharacteristics();
                ArrayList<BluetoothGattCharacteristic> charas =
                        new ArrayList<BluetoothGattCharacteristic>();

                // Loops through available Characteristics.
                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    charas.add(gattCharacteristic);
                    HashMap<String, String> currentCharaData = new HashMap<String, String>();
                    uuid = gattCharacteristic.getUuid().toString();
                    currentCharaData.put(
                            LIST_NAME, GattAttributes.lookup(uuid, unknownCharaString));
                    currentCharaData.put(LIST_UUID, uuid);
                    gattCharacteristicGroupData.add(currentCharaData);
                }
                mGattCharacteristics.add(charas);
                gattCharacteristicData.add(gattCharacteristicGroupData);
            }
        }
    }

    public BluetoothGattDescriptor getDescriptor(BluetoothGattCharacteristic characteristic) {
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        return descriptor;
    }

    public int connectionQueueSize() {
        return connectionQueue.size();
    }

    public int discoveryQueueSize() {
        return serviceDiscoveryQueue.size();
    }

    public int discoveredQueueSize() {
        return serviceDiscoveredQueue.size();
    }

    public int notifyQueueSize() {
        return characteristicNotifyQueue.size();
    }
}