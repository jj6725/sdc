package com.exowear.exowear;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.user.signin.SignInManager;
import com.amazonaws.mobile.user.IdentityManager;
import com.amazonaws.mobile.user.IdentityProvider;

import com.amazonaws.mobile.user.signin.FacebookSignInProvider;

import org.w3c.dom.Text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Activity for signing in users through email or Facebook authentication.
 * Current process:
 * Facebook login - obtains users name to be used in primary key for DynamoDB
 * Email login    - users can input any email and uses that email for primary key for DynamoDB
 */
public class SignInActivity extends Activity {

    /** Provided by AWS Mobile Hub. */
    private final static String LOG_TAG = SignInActivity.class.getSimpleName();
    private SignInManager signInManager;

    /** General variables for android widgets/text animation. */
    private AlphaAnimation buttonClick = new AlphaAnimation(1.0F, 0.7F);
    private TextView tutorialText1;
    private ImageView helpIcon;
    private EditText emailEditText;
    private EditText passEditText;
    private Button signIn;


    /** Permission Request Code (Must be < 256). */
    private static final int GET_ACCOUNTS_PERMISSION_REQUEST_CODE = 93;

    /** The Google OnClick listener, since we must override it to get permissions on Marshmallow and above. */
    private View.OnClickListener googleOnClickListener;

    /**
     * SignInResultsHandler handles the final result from sign in. Making it static is a best
     * practice since it may outlive the SplashActivity's life span.
     */
    private class SignInResultsHandler implements IdentityManager.SignInResultsHandler {
        /**
         * Receives the successful sign-in result and starts the main activity.
         * @param provider the identity provider used for sign-in.
         */
        @Override
        public void onSuccess(final IdentityProvider provider) {
            Log.d(LOG_TAG, String.format("User sign-in with %s succeeded",
                provider.getDisplayName()));

            // The sign-in manager is no longer needed once signed in.
            SignInManager.dispose();

//            Toast.makeText(SignInActivity.this, String.format("Sign-in with %s succeeded.",
//                provider.getDisplayName()), Toast.LENGTH_LONG).show();

            // Load user name and image.
            AWSMobileClient.defaultMobileClient()
                .getIdentityManager().loadUserInfoAndImage(provider, new Runnable() {
                @Override
                public void run() {
                    Log.d(LOG_TAG, "Launching Main Activity...");
                    startActivity(new Intent(SignInActivity.this, ScanActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    // finish should always be called on the main thread.
                    finish();
                }
            });
        }

        /**
         * Recieves the sign-in result indicating the user canceled and shows a toast.
         * @param provider the identity provider with which the user attempted sign-in.
         */
        @Override
        public void onCancel(final IdentityProvider provider) {
            Log.d(LOG_TAG, String.format("User sign-in with %s canceled.",
                provider.getDisplayName()));

            Toast.makeText(SignInActivity.this, String.format("Sign-in with %s canceled.",
                provider.getDisplayName()), Toast.LENGTH_LONG).show();
        }

        /**
         * Receives the sign-in result that an error occurred signing in and shows a toast.
         * @param provider the identity provider with which the user attempted sign-in.
         * @param ex the exception that occurred.
         */
        @Override
        public void onError(final IdentityProvider provider, final Exception ex) {
            Log.e(LOG_TAG, String.format("User Sign-in failed for %s : %s",
                provider.getDisplayName(), ex.getMessage()), ex);

            final AlertDialog.Builder errorDialogBuilder = new AlertDialog.Builder(SignInActivity.this);
            errorDialogBuilder.setTitle("Sign-In Error");
            errorDialogBuilder.setMessage(
                String.format("Sign-in with %s failed.\n%s", provider.getDisplayName(), ex.getMessage()));
            errorDialogBuilder.setNeutralButton("Ok", null);
            errorDialogBuilder.show();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        /**
         * Sign in handler/functions provided by AWS for Facebook log in.
         */
        signInManager = SignInManager.getInstance(this);

        signInManager.setResultsHandler(this, new SignInResultsHandler());

        // Initialize sign-in buttons.
        signInManager.initializeSignInButton(FacebookSignInProvider.class,
                this.findViewById(R.id.fb_login_button));

        /**
         * Code for the general Activity functions/variables.
         */
        tutorialText1 = (TextView) findViewById(R.id.tuttext1);
        helpIcon = (ImageView) findViewById(R.id.help_icon);
        emailEditText = (EditText) findViewById(R.id.signinIn_editText_email);
        passEditText = (EditText) findViewById(R.id.signinIn_editText_password);
        signIn = (Button) findViewById(R.id.signinIn_imageButton_login);

        helpIcon.setVisibility(View.INVISIBLE);

        /**
         * Code for animating the helper textview. (strings.xml lines 39-44)
         * Text 1: New to ExoWear?
         * Text 2: Try our hassle-free sign in process.
         * Text 3: Just sign in using your email or Facebook.
         * Text 4: Click here for help
         * Text 5: All set! Click sign-in to continue
         */
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tutorialText1.setText(R.string.tut1title);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tutorialText1.setText(R.string.tut1body1);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tutorialText1.setText(R.string.tut1body2);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        helpIcon.setVisibility(View.VISIBLE);
                                        tutorialText1.setText(R.string.tut1body3);
                                    }
                                }, 3000);
                            }
                        }, 3000);
                    }
                }, 3000);
            }
        }, 10000);

        /**
         * Code for generating a random account for users ease of access.
         * Methodology: ex. email: user7619@exowear.co -> AWS DynamoDB primary key
         *              ex. password: Exowear7619
         */
        tutorialText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double random = Math.random() * 100000 + 1;
                emailEditText.setText("user" + (int) random + "@exowear.co");
                passEditText.setText("Exowear" + (int) random);
                tutorialText1.setText(R.string.tut1body4);
                helpIcon.setVisibility(View.INVISIBLE);
                tutorialText1.setClickable(false);
            }
        });

        /**
         * Code for signing in using custom/personal email account.
         */
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                if (!emailEditText.getText().toString().equals("") && !passEditText.getText().toString().equals("")) {
                    if (isEmailValid(emailEditText.getText().toString())) {
                        Intent intent = new Intent(getBaseContext(), ScanActivity.class);
                        intent.putExtra("USER_EMAIL_ACCOUNT", emailEditText.getText().toString());
                        startActivity(intent);
                    } else {
                        helpIcon.setVisibility(View.INVISIBLE);
                        tutorialText1.setText(R.string.tut1body6);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                helpIcon.setVisibility(View.VISIBLE);
                                tutorialText1.setText(R.string.tut1body3);
                            }
                        }, 3000);
                    }
                } else {
                    helpIcon.setVisibility(View.INVISIBLE);
                    tutorialText1.setText(R.string.tut1body5);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            helpIcon.setVisibility(View.VISIBLE);
                            tutorialText1.setText(R.string.tut1body3);
                        }
                    }, 3000);
                }
            }
        });
    }

    /**
     * method is used for checking valid email id format.
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        signInManager.handleActivityResult(requestCode, resultCode, data);
    }

}
