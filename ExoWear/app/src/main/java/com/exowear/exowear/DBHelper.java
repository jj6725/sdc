package com.exowear.exowear;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "exowear.db";
    public static final String TABLE_NAME = "patient_data";
    public static final String USER_COLUMN_ID = "userid";
    public static final String USER_COLUMN_NAME = "name";
    public static final String USER_COLUMN_GENDER = "gender";
    public static final String USER_COLUMN_HEIGHT = "height";
    public static final String USER_COLUMN_WEIGHT = "weight";
    public static final String USER_COLUMN_ETHNICITY = "ethnicity";

    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + USER_COLUMN_ID + " INTEGER PRIMARY KEY autoincrement," + USER_COLUMN_NAME + " TEXT not null," + USER_COLUMN_GENDER
                + " TEXT not null," + USER_COLUMN_HEIGHT + " TEXT not null," + USER_COLUMN_WEIGHT + " TEXT not null," + USER_COLUMN_ETHNICITY
                + " TEXT not null" + ")";

        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void deleteAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME + "'");
        db.close();
    }

    public boolean insertData(String userid, String name, String gender, String height, String weight, String ethnicity) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("userid", userid);
        contentValues.put("name", name);
        contentValues.put("gender", gender);
        contentValues.put("height", height);
        contentValues.put("weight", weight);
        contentValues.put("ethnicity", ethnicity);
        db.insert("patient_data", null, contentValues);
        return true;
    }

    public Cursor getData(int userid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from physicians where id=" + userid + "", null);
        return res;
    }

//    public boolean updateData(Integer userid, String quat1, String quat2, String quat3, String quat4, String quat5,
//                              String quat6, String quat7, String quat8, String quat9, String quat10) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("userid", userid);
//        contentValues.put("quat1", quat1);
//        contentValues.put("quat2", quat2);
//        contentValues.put("quat3", quat3);
//        contentValues.put("quat4", quat4);
//        contentValues.put("quat5", quat5);
//        contentValues.put("quat6", quat6);
//        contentValues.put("quat7", quat7);
//        contentValues.put("quat8", quat8);
//        contentValues.put("quat9", quat9);
//        contentValues.put("quat10", quat10);
//        db.update("quaternion", contentValues, "id = ? ", new String[]{Integer.toString(userid)});
//        return true;
//    }

    public Integer deleteData(Integer userid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("patient_data",
                "id = ? ",
                new String[]{Integer.toString(userid)});
    }

    public ArrayList<String> getAllData() {
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        Cursor res = db.rawQuery(selectQuery, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(USER_COLUMN_ID)));
            array_list.add(res.getString(res.getColumnIndex(USER_COLUMN_NAME)));
            array_list.add(res.getString(res.getColumnIndex(USER_COLUMN_GENDER)));
            array_list.add(res.getString(res.getColumnIndex(USER_COLUMN_HEIGHT)));
            array_list.add(res.getString(res.getColumnIndex(USER_COLUMN_WEIGHT)));
            array_list.add(res.getString(res.getColumnIndex(USER_COLUMN_ETHNICITY)));
            res.moveToNext();
        }
        return array_list;
    }
}