package com.exowear.exowear.demo.nosql;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIndexHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIndexRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

import java.util.List;
import java.util.Map;
import java.util.Set;

@DynamoDBTable(tableName = "exowear-mobilehub-805305975-ExoWearDB")

public class ExoWearDBDO {
    private String _userId;
    //private String _username;
    private String _dur;
    private String _rom;
    private String _eMin;
    private String _eMax;
    private String _eMean;
    private String _eVar;
    private String _eStd;
    private String _fMin;
    private String _fMax;
    private String _fMean;
    private String _fVar;
    private String _fStd;

    @DynamoDBHashKey(attributeName = "userId")
    @DynamoDBAttribute(attributeName = "userId")
    public String getUserId() {
        return _userId;
    }

    @DynamoDBAttribute(attributeName = "duration")
    public String getDuration() {
        return _dur;
    }

    @DynamoDBAttribute(attributeName = "rom")
    public String getRoM() {
        return _rom;
    }

    @DynamoDBAttribute(attributeName = "ext_min")
    public String geteMin() {
        return _eMin;
    }

    @DynamoDBAttribute(attributeName = "ext_max")
    public String geteMax() {
        return _eMax;
    }

    @DynamoDBAttribute(attributeName = "ext_mean")
    public String geteMean() {
        return _eMean;
    }

    @DynamoDBAttribute(attributeName = "ext_var")
    public String geteVar() {
        return _eVar;
    }

    @DynamoDBAttribute(attributeName = "ext_std")
    public String geteStd() {
        return _eStd;
    }

    @DynamoDBAttribute(attributeName = "flx_min")
    public String getfMin() {
        return _fMin;
    }

    @DynamoDBAttribute(attributeName = "flx_max")
    public String getfMax() {
        return _fMax;
    }

    @DynamoDBAttribute(attributeName = "flx_mean")
    public String getfMean() {
        return _fMean;
    }

    @DynamoDBAttribute(attributeName = "flx_var")
    public String getfVar() {
        return _fVar;
    }

    @DynamoDBAttribute(attributeName = "flx_std")
    public String getfStd() {
        return _fStd;
    }

    public void setUserId(final String _userId) {
        this._userId = _userId;
    }

    public void setDur(final String _dur) {
        this._dur = _dur;
    }

    public void setRoM(final String _rom) {
        this._rom = _rom;
    }

    public void seteMin(final String _eMin) {
        this._eMin = _eMin;
    }

    public void seteMax(final String _eMax) {
        this._eMax = _eMax;
    }

    public void seteMean(final String _eMean) {
        this._eMean = _eMean;
    }

    public void seteVar(final String _eVar) {
        this._eVar = _eVar;
    }

    public void seteStd(final String _eStd) {
        this._eStd = _eStd;
    }

    public void setfMin(final String _fMin) {
        this._fMin = _fMin;
    }

    public void setfMax(final String _fMax) {
        this._fMax = _fMax;
    }

    public void setfMean(final String _fMean) {
        this._fMean = _fMean;
    }

    public void setfVar(final String _fVar) {
        this._fVar = _fVar;
    }

    public void setfStd(final String _fStd) {
        this._fStd = _fStd;
    }
}
