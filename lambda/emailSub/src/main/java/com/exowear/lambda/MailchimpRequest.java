package com.exowear.lambda;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MailchimpRequest {
    @JsonProperty("email_address")
    private String emailAddress;
    private String status;
    @JsonProperty("merge_fields")
    private Map<String, String> mergeFields;
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Map<String, String> getMergeFields() {
        return mergeFields;
    }
    public void setMergeFields(Map<String, String> mergeFields) {
        this.mergeFields = mergeFields;
    }
}
