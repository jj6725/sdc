package com.exowear.lambda;

public class EmailSubscriptionResponse {
    private boolean isSubscribed;
    private boolean isLogged;
    public boolean isSubscribed() {
        return isSubscribed;
    }
    public void setSubscribed(boolean isSubscribed) {
        this.isSubscribed = isSubscribed;
    }
    public boolean isLogged() {
        return isLogged;
    }
    public void setLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }
}
