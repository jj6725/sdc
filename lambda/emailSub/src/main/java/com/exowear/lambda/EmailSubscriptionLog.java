package com.exowear.lambda;

import java.util.Date;
import java.util.UUID;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "subscriptions")
public class EmailSubscriptionLog {
    private UUID id;
    private String emailAddress;
    private String firstName;
    private String lastName;
    private boolean isSubscribedSuccessfully;
    private Date subscriptionDate;
    private String listId;
    private String source;
    
    @DynamoDBHashKey(attributeName = "ID")
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "EmailAddress")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @DynamoDBAttribute(attributeName = "FirstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @DynamoDBAttribute(attributeName = "LastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @DynamoDBAttribute(attributeName = "SubscribedSuccessfully")
    public boolean isSubscribedSuccessfully() {
        return isSubscribedSuccessfully;
    }

    public void setSubscribedSuccessfully(boolean isSubscribedSuccessfully) {
        this.isSubscribedSuccessfully = isSubscribedSuccessfully;
    }

    @DynamoDBAttribute(attributeName = "SubscriptionDate")
    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }
    
    @DynamoDBAttribute(attributeName = "ListID")
    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    @DynamoDBAttribute(attributeName = "Source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    
}
