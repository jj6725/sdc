package com.exowear.lambda;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.json.Jackson;

public class SubscribeEmail implements RequestHandler<EmailSubscriptionRequest, EmailSubscriptionResponse> {

    private static final String MAILCHIMP_BASE_ENDPOINT = "https://us13.api.mailchimp.com/";
    private static final String MAILCHIMP_API_KEY = "2bf333cbe33c9b17968e477ec5ded3ec-us13";
    
    private MailchimpRequest mailchimpRequestFromEmailSubscriptionRequest(EmailSubscriptionRequest req) {
        MailchimpRequest mcReq = new MailchimpRequest();
        mcReq.setEmailAddress(req.getEmailAddress());
        mcReq.setMergeFields(new HashMap<String, String>());
        mcReq.getMergeFields().put("FNAME", req.getFirstName());
        mcReq.getMergeFields().put("LNAME", req.getLastName());
        mcReq.setStatus("subscribed");
        return mcReq;
    }
    
    private HttpContext getHttpContext() {
        HttpHost host = new HttpHost("us13.api.mailchimp.com", 443, "https");
        CredentialsProvider cp = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("exowear", MAILCHIMP_API_KEY);
        cp.setCredentials(AuthScope.ANY, credentials);
        AuthCache authCache = new BasicAuthCache();
        authCache.put(host, new BasicScheme());
        
        final HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(cp);
        context.setAuthCache(authCache);
        return context;
    }
    
    private boolean subscribeToMailchimp(EmailSubscriptionRequest req, LambdaLogger logger){

        
        HttpClient client = HttpClientBuilder.create().build();
        String url = String.format("%s%s%s%s", MAILCHIMP_BASE_ENDPOINT, "3.0/lists/", req.getListId(), "/members/");

        HttpPost subscribeWithMailchimp = new HttpPost(url);
        subscribeWithMailchimp.addHeader("Content-Type", "application/json");

        MailchimpRequest mcReq = mailchimpRequestFromEmailSubscriptionRequest(req);
        String json = Jackson.toJsonString(mcReq);
        StringEntity stringEntity = null;
        try {
            stringEntity = new StringEntity(json);
            subscribeWithMailchimp.setEntity(stringEntity);
        } catch (UnsupportedEncodingException e1) {
            return false;
        } 
        try {
            HttpContext context = getHttpContext();
            HttpResponse resp = client.execute(subscribeWithMailchimp, context);
        } catch (ClientProtocolException e) {
            logger.log("Experienced ClientProtocolException.");
            return false;
        } catch (IOException e) {
            logger.log("Experienced IOException.");
            return false;
        }
        return true;
    }
    
    private boolean logInDynamo(EmailSubscriptionRequest req, LambdaLogger logger, boolean isSubscribedSuccessfully){
        AmazonDynamoDBClient client = new AmazonDynamoDBClient(new EnvironmentVariableCredentialsProvider());
        client.withRegion(Regions.US_EAST_1);
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        
        EmailSubscriptionLog log = new EmailSubscriptionLog();
        log.setEmailAddress(req.getEmailAddress());
        log.setFirstName(req.getFirstName());
        log.setLastName(req.getLastName());
        log.setId(UUID.randomUUID());
        log.setSubscribedSuccessfully(isSubscribedSuccessfully);
        log.setSubscriptionDate(new Date());
        log.setListId(req.getListId());
        log.setSource(req.getSource());
        try{
            mapper.save(log);            
        }catch (RuntimeException e){
            logger.log(e.getMessage());
            return false;
        }
        return true;
    }
    
    @Override
    public EmailSubscriptionResponse handleRequest(EmailSubscriptionRequest req, Context context) {
        LambdaLogger logger = null;
        if (context != null) {
            logger = context.getLogger();
        }
        EmailSubscriptionResponse response = new EmailSubscriptionResponse();
        if (req.getEmailAddress() == null) {            
            return response;
        }
        response.setSubscribed(subscribeToMailchimp(req, logger));
        response.setLogged(logInDynamo(req, logger, response.isSubscribed()));
        return response;
    }
}
